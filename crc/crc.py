#!/usr/bin/env python3.6
import functools
import operator

def to_binary(number):
    """int -> [int]"""
    if number < 0:
        raise ValueError("Negative number")
    if number == 0:
        return []
    return to_binary(number // 2) + [number % 2]

def lfill(new_length, fill_elem, collection):
    if new_length <= len(collection):
        return collection
    return [fill_elem] + lfill(new_length-1, fill_elem, collection)

def chr_to_binary(char):
    """
    char -> [int]
    must be 8 bit
    """
    if not char:
        return []
    ascii_code = ord(char)
    if ascii_code not in range(0, 256):
        raise ValueError("Not in 8bit range")
    return lfill(8, 0, to_binary(ascii_code))

def foldl(fun, acc, collection):
    """just for the right way"""
    return functools.reduce(fun, collection, acc)

def str_to_binary(string):
    return foldl(operator.add, [],
                 map(lambda x: chr_to_binary(x), string))

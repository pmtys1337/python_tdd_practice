#!/usr/bin/env python3

import unittest
import crc

class TestCRC(unittest.TestCase):
    #test to_binary
    def test_to_binary_not_negative(self):
        self.assertEqual(crc.to_binary(11), [1,0,1,1])
        self.assertEqual(crc.to_binary(42), [1,0,1,0,1,0])
        self.assertEqual(crc.to_binary(127), [1,1,1,1,1,1,1])
        self.assertEqual(crc.to_binary(0), [])

    def test_to_binary_negative(self):
        self.assertRaises(ValueError, crc.to_binary, -11)
        self.assertRaises(ValueError, crc.to_binary, -42)
        self.assertRaises(ValueError, crc.to_binary, -127)

    def test_to_binary_is_binary(self):
        self.assertTrue(all(map(lambda x: x in [0,1], crc.to_binary(11))))
        self.assertTrue(all(map(lambda x: x in [0,1], crc.to_binary(42))))
        self.assertTrue(all(map(lambda x: x in [0,1], crc.to_binary(127))))

    #test lfill
    def test_lfill_below(self):
        self.assertEqual(crc.lfill(3,0,[]), [0,0,0])
        self.assertTrue(len(crc.lfill(3,0,[])) == 3)
        self.assertEqual(crc.lfill(3,0,[1]), [0,0,1])
        self.assertTrue(len(crc.lfill(3,0,[1])) == 3)
        self.assertEqual(crc.lfill(3,0,[1,2]), [0,1,2])
        self.assertTrue(len(crc.lfill(3,0,[1,2])) == 3)

    def test_lfill_not_below(self):
        self.assertEqual(crc.lfill(3,0,[1,2,3]), [1,2,3])
        self.assertTrue(len(crc.lfill(3,0,[1,2,3])) == 3)
        self.assertEqual(crc.lfill(3,0,[1,2,3,4]), [1,2,3,4])
        self.assertTrue(len(crc.lfill(3,0,[1,2,3,4])) == 4)

    #test chr_to_binary
    def test_chr_to_binary_must_be_eight_bit(self):
        self.assertTrue(len(crc.chr_to_binary("a")) == 8)
        self.assertTrue(len(crc.chr_to_binary("A")) == 8)
        self.assertTrue(len(crc.chr_to_binary("!")) == 8)
        self.assertTrue(len(crc.chr_to_binary("~")) == 8)
        self.assertTrue(len(crc.chr_to_binary("\t")) == 8)
        self.assertTrue(len(crc.chr_to_binary("\n")) == 8)
        self.assertRaises(ValueError, crc.chr_to_binary, "€")

    def test_chr_to_binary(self):
        self.assertEqual(crc.chr_to_binary(""), [])
        self.assertEqual(crc.chr_to_binary("\n"), [0, 0, 0, 0, 1, 0, 1, 0])
        self.assertEqual(crc.chr_to_binary("L"), [0, 1, 0, 0, 1, 1, 0, 0])
        self.assertEqual(crc.chr_to_binary("~"), [0, 1, 1, 1, 1, 1, 1, 0])

    def test_str_to_binary(self):
        self.assertEqual(crc.str_to_binary(""), [])
        self.assertEqual(crc.str_to_binary("H"), [0, 1, 0, 0, 1, 0, 0, 0])
        self.assertEqual(crc.str_to_binary("Hello World!"),
                         [0, 1, 0, 0, 1, 0, 0, 0,
                          0, 1, 1, 0, 0, 1, 0, 1,
                          0, 1, 1, 0, 1, 1, 0, 0,
                          0, 1, 1, 0, 1, 1, 0, 0,
                          0, 1, 1, 0, 1, 1, 1, 1,
                          0, 0, 1, 0, 0, 0, 0, 0,
                          0, 1, 0, 1, 0, 1, 1, 1,
                          0, 1, 1, 0, 1, 1, 1, 1,
                          0, 1, 1, 1, 0, 0, 1, 0,
                          0, 1, 1, 0, 1, 1, 0, 0,
                          0, 1, 1, 0, 0, 1, 0, 0,
                          0, 0, 1, 0, 0, 0, 0, 1])

if __name__ == "__main__":
    unittest.main()
